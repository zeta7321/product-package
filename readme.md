# product package #

![4.9.3](https://img.shields.io/badge/version-4.9.3-green.svg)

Выбери упаковку


### Details ###

Данный модуль позвоялет создать выбор упаковки товара

* Developed for CS-CART EDITION 4.9.3

### What should I check? ###

* Проверь настройку в товаре

### Questions? ###

* Developer: Kutyumov Andrey
* Manager: Sergei Minyukevich